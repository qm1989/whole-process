const state = {
  cache: []
}

const mutations = {
  addCache(state, name) {
    if (!state.cache.includes(name)) {
      state.cache.push(name)
    }
  },
  removeCache(state, name) {
    let index = state.cache.findIndex(item => item === name)
    if (index >= 0) {
      state.cache.splice(index, 1)
    }
  }
}

const actions = {}

export default {
  state,
  mutations,
  actions
}
