// import Container from '@/components/common/Container'
// 测试部分
export const testRouter = [
  {
    path: "/checkarea",
    name: "checkarea",
    meta: {
      title: "复选框区域选择"
    },
    component: () => import("@/components/common/CheckArea")
  }
];

export const loginRouter = {
  path: "/login",
  name: "login",
  meta: {
    title: "登录"
  },
  component: () => import("@/components/server/Login")
};

export const registerRouter = {
  path: "/register",
  name: "register",
  meta: {
    title: "注册"
  },
  component: () => import("@/components/server/Register")
};

export const psRouter = {
  path: "/modifypw",
  name: "modifypw",
  meta: {
    title: "修改密码"
  },
  component: () => import("@/components/server/ModifyPw")
};

export const prompRouter = {
  path: "/promp",
  name: "promp",
  meta: {
    title: "未开通提示"
  },
  component: () => import("@/components/common/Promp")
};

export const errorRouter = {
  path: "/*",
  name: "error-404",
  meta: {
    title: "404-页面不存在"
  },
  component: () => import("@/components/error-page/ErrorPage")
};

// 页头顶部导航(便于书写)
export const headerRouter = [
         // {
         //   path: '/home',
         //   name: 'first-content',
         //   redirect: {
         //     name: 'works-chedule'
         //   },
         //   meta: {
         //     title: '首页',
         //   },
         //   icon: 'static/logo_index.png',
         //   component: Container
         // },
         {
           path: "/check-static",
           name: "check-static",
           meta: {
             title: "进度查询",
             permission: [1, 3]
           },
           component: () => import("@/components/check-statistics/CheckStatic")
         },
         {
           path: "/workdesk",
           name: "workdesk",
           meta: {
             title: "三调流程",
             permission: [1, 3]
           },
           component: () => import("@/components/work-desk/WorkDesk")
         },
         {
           path: "/usermanager",
           name: "usermanager",
           meta: {
             title: "人员设备"
           },
           redirect: {
             name: "userstatistics"
           },
           component: () => import("@/components/user-manager/userManager"),
           children: [
             {
               path: "userstatistics",
               name: "userstatistics",
               meta: {
                 title: "人员统计"
               },
               component: () =>
                 import("@/components/user-manager/UserStatistics")
             },
             {
               path: "adminuser",
               name: "adminuser",
               meta: {
                 title: "工作进度"
               },
               component: () => import("@/components/user-manager/AdminUser")
             },
             {
               path: "signmanager",
               name: "signmanager",
               meta: {
                 title: "签到管理"
               },
               component: () => import("@/components/supervise/SignManager")
             },
             {
               path: "signtrajetory",
               name: "signtrajetory",
               meta: {
                 title: "签到轨迹"
               },
               component: () => import("@/components/supervise/SignTrajetory")
             },
             {
               path: "thirdsurvey",
               name: "thirdsurvey",
               meta: {
                 title: "三调办"
               },
               component: () => import("@/components/supervise/ThirdSurvey"),
               redirect: {
                 name: "province"
               },
               children: [
                 {
                   path: "province",
                   name: "province",
                   meta: {
                     title: "省级三调办"
                   },
                   component: () =>
                     import("@/components/supervise/third-survey/Province")
                 },
                 {
                   path: "city",
                   name: "city",
                   meta: {
                     title: "地市三调办"
                   },
                   component: () =>
                     import("@/components/supervise/third-survey/City")
                 },
                 {
                   path: "country",
                   name: "country",
                   meta: {
                     title: "区县三调办"
                   },
                   component: () =>
                     import("@/components/supervise/third-survey/Country")
                 }
               ]
             },
             {
               path: "workunit",
               name: "workunit",
               meta: {
                 title: "作业单位"
               },
               component: () => import("@/components/supervise/WorkUnit"),
               // redirect: {
               //   name: "workprovince"
               // },
               children: [
                 {
                   path: "workprovince",
                   name: "workprovince",
                   meta: {
                     title: "省级作业单位"
                   },
                   component: () =>
                     import("@/components/supervise/work-unit/WorkProvince")
                 },
                 {
                   path: "workcity",
                   name: "workcity",
                   meta: {
                     title: "地市作业单位"
                   },
                   component: () =>
                     import("@/components/supervise/work-unit/WorkCity")
                 },
                 {
                   path: "workcountry",
                   name: "workcountry",
                   meta: {
                     title: "区县作业单位"
                   },
                   component: () =>
                     import("@/components/supervise/work-unit/WorkCountry")
                 }
               ]
             }
           ]
         },
         {
           path: "/statisticsmanager",
           name: "statisticsmanager",
           meta: {
             title: "作业过程",
             permission: [1]
           },
           availTypes: [1],
           redirect: {
             name: "overall-progress"
           },
           component: () =>
             import("@/components/statistics-spot/StatisticsManager"),
           children: [
             {
               path: "works-chedule",
               name: "works-chedule",
               meta: {
                 title: "外网作业过程监管"
               },
               component: () =>
                 import("@/components/statistics-spot/WorksChedule")
             },
             {
               path: "assessor-statis",
               name: "assessor-statis",
               meta: {
                 title: "外业审核进度监管"
               },
               component: () =>
                 import("@/components/statistics-spot/AssessorStatis")
             },
             {
               path: "work-statistics",
               name: "work-statistics",
               meta: {
                 title: "外网核查工作监管"
               },
               component: () =>
                 import("@/components/statistics-spot/WorkStatistics")
             },
             {
               path: "overall-progress",
               name: "overall-progress",
               meta: {
                 title: "外业总体进度监管"
               },
               component: () =>
                 import("@/components/statistics-spot/OverallProgress")
             },
             {
               path: "lifecycle",
               name: "lifecycle",
               meta: {
                 title: "图斑生命周期监管"
               },
               component: () => import("@/components/statistics-spot/Lifecycle")
             },
             {
               path: "qutsideQuality",
               name: "qutsideQuality",
               meta: {
                 title: "外业人员工作质量"
               },
               component: () =>
                 import("@/components/statistics-spot/OutsideQuality")
             }
           ]
         },
         {
           path: "/progressstatistics",
           name: "progressstatistics",
           meta: {
             title: "进度统计",
             permission: [1, 3]
           },
           redirect: {
             name: "countylevelFunding-statistics"
           },
           component: () =>
             import("@/components/progress-statistics/ProgressStatistics"),
           children: [
             {
               path: "countylevelFunding-statistics",
               name: "countylevelFunding-statistics",
               meta: {
                 title: "调查经费落实统计"
               },
               component: () =>
                 import(
                   "@/components/progress-statistics/CountylevelFundingStatistics"
                 )
             },
             {
               path: "implementation-statistics",
               name: "implementation-statistics",
               meta: {
                 title: "实施方案备案统计"
               },
               component: () =>
                 import(
                   "@/components/progress-statistics/ImplementationStatistics"
                 )
             },
             {
               path: "countylevel-processing",
               name: "countylevel-processing",
               meta: {
                 title: "县级内业处理统计"
               },
               component: () =>
                 import(
                   "@/components/progress-statistics/CountylevelProcessing"
                 )
             },
             {
               path: "foreigntrade-county",
               name: "foreigntrade-county",
               meta: {
                 title: "县级外业调查统计"
               },
               component: () =>
                 import("@/components/progress-statistics/ForeigntradeCounty")
             },
             {
               path: "countylevelLandType-statistics",
               name: "countylevelLandType-statistics",
               meta: {
                 title: "地类样本统计"
               },
               component: () =>
                 import(
                   "@/components/progress-statistics/CountylevelLandTypeStatistics"
                 )
             },
             {
               path: "countylevelFirstProof-statistics",
               name: "countylevelFirstProof-statistics",
               meta: {
                 title: "县级初次举证统计"
               },
               component: () =>
                 import(
                   "@/components/progress-statistics/CountylevelFirstProofStatistics"
                 )
             },
             {
               path: "countylevel-proving",
               name: "countylevel-proving",
               meta: {
                 title: "县级外业举证统计"
               },
               component: () =>
                 import("@/components/progress-statistics/CountylevelProving")
             },
             {
               path: "countylevel-construction",
               name: "countylevel-construction",
               meta: {
                 title: "县级建库统计"
               },
               component: () =>
                 import(
                   "@/components/progress-statistics/CountylevelConstruction"
                 )
             },
             {
               path: "provincelevelInner-statistics",
               name: "provincelevelInner-statistics",
               meta: {
                 title: "省级内业检查统计"
               },
               component: () =>
                 import(
                   "@/components/progress-statistics/ProvincelevelInnerStatistics"
                 )
             },
             {
               path: "provincelevelOuter-statistics",
               name: "provincelevelOuter-statistics",
               meta: {
                 title: "省级外业检查统计"
               },
               component: () =>
                 import(
                   "@/components/progress-statistics/ProvincelevelOuterStatistics"
                 )
             },
             {
               path: "countylevel-achievements",
               name: "countylevel-achievements",
               meta: {
                 title: "县级成果上报统计"
               },
               component: () =>
                 import(
                   "@/components/progress-statistics/CountylevelAchievements"
                 )
             },
             {
               path: "countylevelChcked-statistics",
               name: "countylevelChcked-statistics",
               meta: {
                 title: "县级成果国家检查统计"
               },
               component: () =>
                 import(
                   "@/components/progress-statistics/CountylevelChckedStatistics"
                 )
             },
             {
               path: "provincelevelCreate-statistics",
               name: "provincelevelCreate-statistics",
               meta: {
                 title: "省级建库统计"
               },
               component: () =>
                 import(
                   "@/components/progress-statistics/ProvincelevelCreateStatistics"
                 )
             }
           ]
         },
         {
           path: "/supervise",
           name: "supervise",
           meta: {
             title: "进度上报"
           },
           component: () => import("@/components/progress-report/supervise"),
           redirect: {
             name: "process"
           },
           children: [
             {
               path: "process",
               name: "process",
               meta: {
                 title: "调查工作进度"
               },
               component: () => import("@/components/progress-report/process")
             },
             {
               path: "appropriation",
               name: "appropriation",
               meta: {
                 title: "调查经费落实情况"
               },
               component: () =>
                 import("@/components/progress-report/appropriation")
             },
             {
               path: "deployment",
               name: "deployment",
               meta: {
                 title: "总体部署进度"
               },
               component: () =>
                 import("@/components/progress-report/deployment")
             }
           ]
         },
         {
           path: "/teamworkmanager",
           name: "teamworkmanager",
           meta: {
             title: "多级协同"
           },
           redirect: {
             name: "policylawslist"
           },
           component: () => import("@/components/teamwork/TeamworkManager"),
           children: [
             {
               path: "policylawslist",
               name: "policylawslist",
               meta: {
                 title: "政策法规"
               },
               component: () => import("@/components/teamwork/PolicyLawsList")
             },
             {
               path: "workreport",
               name: "workreport",
               meta: {
                 title: "工作汇报"
               },
               component: () => import("@/components/teamwork/WorkReport")
             },
             {
               path: "noticelist",
               name: "noticelist",
               meta: {
                 title: "公告列表"
               },
               component: () => import("@/components/teamwork/NoticeList")
             },
             {
               path: "remindernotice",
               name: "remindernotice",
               meta: {
                 title: "催办通知"
               },
               component: () => import("@/components/teamwork/ReminderNotice")
             }
           ]
         },
         {
           path: "/systemManage",
           name: "systemManage",
           meta: {
             title: "系统管理",
             permission: [1]
           },
           component: () => import("@/components/system-manage/systemManage"),
           redirect: {
             name: "systemUser"
           },
           children: [
             {
               path: "systemUser",
               name: "systemUser",
               meta: {
                 title: "系统用户"
               },
               component: () => import("@/components/system-manage/systemUser")
             },
             {
               path: "inspectUser",
               name: "inspectUser",
               meta: {
                 title: "核查用户"
               },
               component: () => import("@/components/system-manage/inspectUser")
             },
             {
               path: "logManage",
               name: "logManage",
               meta: {
                 title: "操作日志"
               },
               component: () => import("@/components/system-manage/logManage")
             }
           ]
         },
         {
           path: "/activateBusiness",
           name: "activateBusiness",
           meta: {
             title: "扩展业务",
             permission: [1]
           },
           component: () =>
             import("@/components/activate-business/ActivateBusiness"),
           redirect: {
             name: "yearChange"
           },
           children: [
             {
               path: "yearChange",
               name: "yearChange",
               meta: {
                 title: "年度变更"
               },
               component: () =>
                 import("@/components/activate-business/YearChange")
             },
             {
               path: "landLaw",
               name: "landLaw",
               meta: {
                 title: "土地卫片执法"
               },
               component: () => import("@/components/activate-business/LandLaw")
             },
             {
               path: "supervision",
               name: "supervision",
               meta: {
                 title: "耕地监测监管"
               },
               component: () =>
                 import("@/components/activate-business/Supervision")
             },
             {
               path: "naturalResources",
               name: "naturalResources",
               meta: {
                 title: "资源资源保护区监管"
               },
               component: () =>
                 import("@/components/activate-business/NaturalResources")
             }
           ]
         }
       ];
const supervise = {
  path: "/supervise-hc",
  name: "supervise-hc",
  meta: {
    title: "进度上报"
  },
  component: () => import("@/components/progress-report/supervise-hc"),
  redirect: {
    name: "process-hc"
  },
  children: [
    {
      path: "process-hc",
      name: "process-hc",
      meta: {
        title: "调查工作进度"
      },
      component: () => import("@/components/progress-report/process")
    },
    {
      path: "appropriation-hc",
      name: "appropriation-hc",
      meta: {
        title: "调查经费落实情况"
      },
      component: () => import("@/components/progress-report/appropriation")
    },
    {
      path: "deployment-hc",
      name: "deployment-hc",
      meta: {
        title: "总体部署进度"
      },
      component: () => import("@/components/progress-report/deployment")
    }
  ]
};

export const msgRouter = {
  path: "/msg",
  name: "msg",
  meta: {
    title: "通知消息"
  },
  component: () => import("@/components/system-manage/Message")
};

export const routers = [
  loginRouter,
  supervise,
  registerRouter,
  {
    path: "/",
    meta: {
      title: "应用"
    },
    name: "home",
    component: () => import("@/components/home-page/HomePage"),
    // ...headerRouter,
    children: [
      ...testRouter,
      ...headerRouter,
      psRouter,
      prompRouter,
      msgRouter,
      errorRouter
    ]
  }
];
