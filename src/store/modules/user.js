import axios from 'axios'
let setUser = (state, user) => {
  state.user = user
  // localStorage.userinfo = JSON.stringify(Object.assign(user, { expires: new Date().getTime() })) // eslint-disable-line
}

// 针对jwt验证使用
let setToken = (state) => {
  axios.defaults.headers.common['Authorization'] = `Survey ${state.user.oauth}`
}

const state = {
  user: null,
  isLogin: false
}

const mutations = {
  login (state, user) {
    // console.log('登录的一些操作', user)
    // 针对jwt验证使用
    state.isLogin = true
    setUser(state, user)
    setToken(state)
  },
  loginOut (state) {
    state.user = null
    state.isLogin = false
    localStorage.removeItem('userinfo') // eslint-disable-line
  },
  modifyUser (state, userInfo) {
    // console.log('修改用户操作')
    setUser(state, userInfo)
  },
  setToken(state) {
    // console.log('修改用户操作')
    setToken(state)
  }
}

const actions = {}

export default {
  state,
  mutations,
  actions
}
