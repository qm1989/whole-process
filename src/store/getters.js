const getters = {
  userType: state => state.user.user ? state.user.user.userType : null,
  cityId: state => state.user.user ? state.user.user.cityId : null,
  areaId: state => state.user.user ? state.user.user.areaId : null,
}
export default getters
