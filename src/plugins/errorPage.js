import router from '../router'
import store from '../store'
import {
  Message
} from 'element-ui'

let errorPage = {}

errorPage.install = function (Vue, options) {
  // 接受错误的error
  Vue.catchError = Vue.prototype.$catchError = function (error, ...funcs) {
    // console.log('catchError-->', error, error.response);
    if (!error.response) {
      Message({
        // duration: 0,
        showClose: true,
        message: `错误信息：${error}`,
        type: 'error'
      })
      return
    }
    // debugger
    if (error.response.status === 401) {
      // 跳首页，打开登录
      router.push({
        name: 'login'
      })
    } else {
      // router.push({
      //   path: `/${error.response.status}`
      // })
      Message({
        showClose: true,
        message: `提示信息：状态错误-${error.response.status}`,
        type: 'info'
      })
    }
    funcs.forEach(func => func())
  }
  // 返回正确接收返回非1的错误码
  Vue.codeError = Vue.prototype.$codeError = function (res, ...funcs) {
    // console.log('codeError-->', res, res.data.message);
    if (res.data.message === '请先登录' || res.data.error === 'invalid_token') {
      store.commit('loginOut')
      router.push({
        name: 'login'
      })
      return
    }
    // 返回状态为200，但返回code不是1
    if (res.data.code === 0) {
      Message({
        // duration: 0,
        showClose: true,
        message: `提示信息：${res.data.data}`,
        type: 'info'
      })
    } else {
      Message({
        // duration: 0,
        showClose: true,
        message: `提示信息：${res.data.message}`,
        type: 'info'
      })
    }
    funcs.forEach(func => func())
  }
}
export default errorPage
