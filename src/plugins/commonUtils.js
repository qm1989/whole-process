export default {
    proviceStatusToStr: function(status) {
        var result = "";
        switch (status) {
            case 0:
                result = "全部图斑";
                break;
            case 1:
                result = "市级未上报";
                break;
            case 2:
                result = "待审核";
                break;
            case 3:
                result = "已通过";
                break;
            case 4:
                result = "未通过";
                break;
        }
        return result;
    },
    cityStatusToStr: function(status) {
        var result = "";
        switch (status) {
            case 0:
                result = "全部图斑";
                break;
            case 1:
                result = "县级未上报";
                break;
            case 2:
                result = "待审核";
                break;
            case 3:
                result = "已通过";
                break;
            case 4:
                result = "未通过";
                break;
            case 5:
                result = "市级已上报";
                break;
        }
        return result;
    },
    getTotalArea: function() {
        return 107
    },
    provinceCenter: [108.773949, 35.601077],
    cityScal: [{
        cityId: "610100",
        cityName: "西安市",
        cityPoint: [],
        cityScal: 2000000
    }, {
        cityId: "610200",
        cityName: "铜川市",
        cityPoint: [],
        cityScal: 1000000
    }, {
        cityId: "610300",
        cityName: "宝鸡市",
        cityPoint: [],
        cityScal: 2000000
    }, {
        cityId: "610400",
        cityName: "咸阳市",
        cityPoint: [],
        cityScal: 1500000
    }, {
        cityId: "610500",
        cityName: "渭南市",
        cityPoint: [],
        cityScal: 2000000
    }, {
        cityId: "610600",
        cityName: "延安市",
        cityPoint: [],
        cityScal: 2000000
    }, {
        cityId: "610700",
        cityName: "汉中市",
        cityPoint: [],
        cityScal: 2000000
    }, {
        cityId: "610800",
        cityName: "榆林市",
        cityPoint: [],
        cityScal: 3000000
    }, {
        cityId: "610900",
        cityName: "安康市",
        cityPoint: [],
        cityScal: 2000000
    }, {
        cityId: "611000",
        cityName: "商洛市",
        cityPoint: [],
        cityScal: 2000000
    }]
}