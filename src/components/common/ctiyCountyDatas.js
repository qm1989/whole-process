import Vue from 'vue';
import axios from 'axios'
// 过期时效
// let time = 0

let loginUer = ''
let citys = [{
  cityId: '全部',
  cityName: '全部',
  areaDatas: []
}];
export default async function (vm, url) {
  try {
    // 需要时效缓存时放开
    // let newTime = new Date().getTime()
    // if (newTime - time < 1000 * 60 * 12) {
    //   return datas
    // }
    if (citys.length > 1 && loginUer === vm.$store.state.user.user.account) {
      return citys
    }
    
    // 为防止ie对此请求的缓存，特意加了时间戳
    // let res = await axios.get('survey/areaInfo/getExpertArea?timestamp=' + new Date().getTime())
    let res = await axios.get(url, {})
    // console.log('res-->', res);
    if (res.data.code !== '200') {
      Vue.codeError(res)
      return []
    }
    citys = [{
      cityId: '',
      cityName: '全部',
      areaDatas: []
    }];
    loginUer = vm.$store.state.user.user.account
    if (res.data.data instanceof Array) {
      datas.forEach(item => {
        let index = citys.findIndex(element => element.cityId === item.cityId)
        if (index > -1) { // 有相同的，将县加入即可
          citys[index].arears.push({
            areaName: item.areaName,
            areaId: item.areaId
          })
        } else {
          citys.push({
            cityId: item.cityId,
            cityName: item.cityName,
            areaDatas: [{
              areaId: '',
              areaName: '全省'
            }, {
              areaName: item.areaName,
              areaId: item.areaId
            }]
          })
        }
      })
    } else {
      citys = [...citys, ...res.data.data.citys.map(city => {
        city.areaDatas.unshift({
          areaId: '',
          areaName: '全部'
        })
        return city
      })]
    }
    return citys
  } catch (error) {
    Vue.catchError(error)
  }

}