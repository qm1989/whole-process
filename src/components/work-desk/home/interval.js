import Echarts from "echarts";
import axios from 'axios'

export default {
  data() {
    return {
      interval: null
    }
  },
  mounted() {
    this.init();
    // this.loopData()
    this.getData()
  },
  methods: {
    init() {
      let dom = this.$refs.echarts;
      this.chart = Echarts.init(dom);
      window.addEventListener("resize", this.chart.resize);
    },
    loopData() {
      this.getData()
      this.interval = setInterval(() => {
        this.getData()
      }, 1000 * 60 * 10);
    },
    getData() {
      this.$get(`${this.url}`).then(res => {
        this.setData(res.data.data)
      })
    }
  },
  beforeDestroy() {
    window.removeEventListener('resize', this.chart.resize)
    if (this.interval) {
      clearInterval(this.interval)
    }
  }
}
