import { headerRouter } from '@/router/router'
import router from '@/router'
const state = {
  headerRouter: []
}

// 对顶部数据进行过滤，将不能使用的路由去掉
// 要解决多级过滤问题，当有子元素，且子元素的所有都不满足时，就将父元素也过滤掉
let filterPage = (routers, type) => {
    let end = routers.filter(item => {
      if (item.children && item.children.length > 0) {

        let newChildren = filterPage(item.children)
        if (newChildren.length === 0 || !isUsed(item.meta && item.meta.permission)) {
          return false
        }
        item.children = newChildren
        return true
      }
      return isUsed(item.meta && item.meta.permission, type)
    })
    return end
  }

  // 当前登录用户是否可用
  let isUsed = (types, type) => {
    // 没有传入可用的类型和可用类型数组没有元素，统一视为不设限制
    if (!types || types.length === 0) {
      return true
    }
    return types.some(item => parseInt(item) === parseInt(type))
  }

const mutations = {
  setRouter(state, type) {
    let routers = filterPage(headerRouter, type)
    state.headerRouter = routers
    router.options.routes[3].children.unshift(routers)
    router.addRoutes(routers)
    console.log('22222222222222222222', router);
    
    // router.options.routes
    
  }
}

export default {
  state,
  mutations
}
