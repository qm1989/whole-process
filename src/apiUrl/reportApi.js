import request from 'axios'

const report = {
	getStsPolicy: (params) => request({
		url: '/survey/aliyun/oss/stsPolicy',
		method: 'get',
		params
    }),
    getProgressReport: (params) => request({
		url: '/survey/report/findReportWorkProgress',
		method: 'get',
		params
    }),
    getAppropriationReport: (params) => request({
		url: '/survey/report/findSurveyOutlayImplement',
		method: 'get',
		params
    }),
    getDeploymentReport: (params) => request({
		url: '/survey/report/findSurveyOverallDeploy',
		method: 'get',
		params
	}),
	saveProgressReport: (data) => request({
		url: '/survey/report/reportWorkProgress',
		method: 'post',
		data: data
    }),
    saveAppropriationReport: (data) => request({
		url: '/survey/report/surveyOutlayImplement',
		method: 'post',
		data: data
    }),
    saveDeploymentReport: (data) => request({
		url: '/survey/report/surveyOverallDeploy',
		method: 'post',
		data: data
    }),
	delAttachmentlog: (params) => request({
		url: '/survey/supspot/delAttachmentlog',
		method: 'get',
		params
	}),
}

export default report