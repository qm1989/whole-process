import Vue from 'vue'
const state = {
  message: {
    data: [],
    pageInfo: {}
  }
}

const mutations = {
  setMessage(state, msg) {
    state.message.data = msg.data
    state.message.pageInfo = msg.pageInfo
  }
}

const actions = {
  getMessage(context) {
    Vue.get('survey/message/receives?currentPage=1&pageSize=3&stage=1').then(res => {
      context.commit('setMessage', res.data)
    })
  }
}

export default {
  state,
  mutations,
  actions
}
