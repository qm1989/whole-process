import Vue from 'vue'
import Viewer from 'v-viewer'
import 'viewerjs/dist/viewer.css'

Vue.use(Viewer)

// 本插件只针对单张图片的点击放大,暂未支持多张图片切换,如要多张切换,请使用`v-viewer`
let ImgInstance = Vue.extend({
  components: { Viewer },
  template: `
    <div>
      <viewer :images="[image]" @inited="inited" class="viewer" ref="viewer" :options="option">
        <template slot-scope="scope">
          <span>{{scope.images}}</span>
          <img v-for="(src, index) in scope.images" :src="src" :key="index">
        </template>
      </viewer>
    </div>
  `,
  data () {
    return {
      image: null,
      option: {
        navbar: 0,
        toolbar: {
          zoomIn: 4,
          zoomOut: 4,
          oneToOne: 4,
          reset: 4,
          rotateLeft: 4,
          rotateRight: 4
        }
      }
    }
  },
  methods: {
    inited (viewer) {
      this.viewer = viewer
    },
    showImg (url) {
      this.image = url
      setTimeout(() => {
        this.viewer.show()
      }, 0)
    }
  }
})
const viwer = new ImgInstance({
  el: document.createElement('div')
})
Vue.prototype.$showImg = viwer.showImg // eslint-disable-line