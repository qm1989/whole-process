
import store from '@/store'

export default {
  inserted(el, binding, vnode) {
    const { value } = binding
    // 1.获取当前登录用户的角色
    // 2.拿到传进来的可用用户角色数组
    // 3.判断当前登录用户角色在不在可用用户角色数组中
    const userType = store.getters && store.getters.userType
    if (value && value instanceof Array && value.length > 0) {
      const permissionType = value
      const hasPermission = permissionType.some(item => parseInt(item) === parseInt(userType))
      if (!hasPermission) {
        el.parentNode && el.parentNode.removeChild(el)
      }
    } else {
      throw new Error(`需要传入可操作的用户类型值! 例如 v-permission="[1, 2, 3, 4]"`)
    }
  }
}
