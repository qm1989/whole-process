import Vue from 'vue'
import { Message } from 'element-ui'

/**
 * 1. native 开头的只适合原生元素
 * <input placeholder="金额" NativeNumber ></input>
 * 因为el-input是被封过过的,实际解析后是一个div包着一个input
 * 
 * 2. el.dispatchEvent(new Event('input'));
 * vue语法糖其实监控的是input事件,但是直接e.value不触发input,所以造成 v-model双向绑定无效.需要手动调用触发input一次
 * @author wesson
 */

/**
 * v-nativeNumber
 * 只能输入数字和小数点
 */
Vue.directive('nativeNumber', {
    bind: function (el, binding, vnode) {
        el.handler = function () {
            el.value = el.value.replace(/[^\.\d]/g, '')
        }
        el.addEventListener('input', el.handler)
    },
    unbind: function (el) {
        el.removeEventListener('input', el.handler)
    }
})
/**
 * v-nativeInteger
 * 只能输入整数
 */
Vue.directive('nativeInteger', {
    bind: function (el, binding, vnode) {
        el.handler = function () {
            this.value = this.value.replace(/[^\d]/g, '')
        }
        el.addEventListener('input', el.handler)
    },
    unbind: function (el) {
        el.removeEventListener('input', el.handler)
    }
})

/**
 * v-nativePhone
 * 只能输入整数,离开时验证手机号码格式
 */
Vue.directive('nativePhone', {
    bind: function (el, binding, vnode) {
        el.handler = function () {
            console.log(el)
            el.value = el.value.replace(/[^\d]/g, '')
        }
        el.blurhandler = function () {
            console.log(el)
            let reg = /^1[3|4|5|7|8|9][0-9]{9}$/;
            if(this.value != "" && !reg.test(this.value)){
                Message.error("输入的手机号码不正确");
                el.value=""
                el.dispatchEvent(new Event('input'));
            }
        }
        el.addEventListener('input', el.handler)
        el.addEventListener('change', el.blurhandler)
    },
    unbind: function (el) {
        el.removeEventListener('input', el.handler)
        el.removeEventListener('change', el.blurhandler)
    }
})

/**
 * v-nativeMoney
 * 只能输入数字,小数. 默认0输入时为空,离开为空重置为0
 */
Vue.directive('nativeMoney', {
    bind: function (el, binding, vnode) {
        el.handler = function () {
            el.value = el.value.replace(/[^\.\d]/g, '')
        }
        el.focusHandler = function () {
            let val = el.value;
            if (val === "0") {
                el.value = "";
                el.dispatchEvent(new Event('input'));
            }
        }
        el.blurhandler = function () {
            let val = el.value;
            if (val === "") {
                el.value = "0";
                el.dispatchEvent(new Event('input'));
            }
        }
        el.addEventListener('input', el.handler)
        el.addEventListener('focus', el.focusHandler)
        el.addEventListener('blur', el.blurhandler)
    },
    unbind: function (el) {
        el.removeEventListener('input', el.handler)
        el.addEventListener('focus', el.focusHandler)
        el.addEventListener('blur', el.blurhandler)
    }
})

/**
 * v-nativePhone
 * 只能输入整数,离开时验证手机号码格式
 */
Vue.directive('nativeIdCard', {
    bind: function (el, binding, vnode) {
        el.handler = function () {
            console.log(el)
            el.value = el.value.replace(/[^\d|X|x]/g, '')
        }
        el.blurhandler = function () {
            console.log(el)
            let reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
            if(this.value != "" && !reg.test(this.value)){
                Message.error("输入的身份证号码有误,请重新输入");
                el.value=""
                el.dispatchEvent(new Event('input'));
            }
        }
        el.addEventListener('input', el.handler)
        el.addEventListener('change', el.blurhandler)
    },
    unbind: function (el) {
        el.removeEventListener('input', el.handler)
        el.removeEventListener('change', el.blurhandler)
    }
})